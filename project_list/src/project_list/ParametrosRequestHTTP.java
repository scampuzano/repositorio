package project_list;



import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.CredentialsProvider;

public class ParametrosRequestHTTP {
    public static final int GET = 1;
    public static final int POST = 2;
    private Map<String, String> cookies;
    private CredentialsProvider credenciales;
    private boolean descargar;
    private HeadersListener headersListener;
    private Map<String, String> headersParametros;
    private boolean isPostData;
    private List<NameValuePair> parametros;
    private String postData;
    private int tipoSolicitud = 1;
    private String url;

    public int getTipoSolicitud() {
        return this.tipoSolicitud;
    }

    public void setTipoSolicitud(int tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public List<NameValuePair> getParametros() {
        return this.parametros;
    }

    public void setParametros(List<NameValuePair> parametros) {
        this.parametros = parametros;
    }

    public Map<String, String> getCookies() {
        return this.cookies;
    }

    public void setCookies(Map<String, String> cookies) {
        this.cookies = cookies;
    }

    public CredentialsProvider getCredenciales() {
        return this.credenciales;
    }

    public void setCredenciales(CredentialsProvider credenciales) {
        this.credenciales = credenciales;
    }

    public Map<String, String> getHeadersParametros() {
        return this.headersParametros;
    }

    public void setHeadersParametros(Map<String, String> headersParametros) {
        this.headersParametros = headersParametros;
    }

    public boolean isPostData() {
        return this.isPostData;
    }

    public void setPostData(boolean isPostData) {
        this.isPostData = isPostData;
    }

    public String getPostData() {
        return this.postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public boolean isDescargar() {
        return this.descargar;
    }

    public void setDescargar(boolean descargar) {
        this.descargar = descargar;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HeadersListener getHeadersListener() {
        return this.headersListener;
    }

    public void setHeadersListener(HeadersListener headersListener) {
        this.headersListener = headersListener;
    }
}
