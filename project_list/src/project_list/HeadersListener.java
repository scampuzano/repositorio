package project_list;


import org.apache.http.Header;

public interface HeadersListener {
    void dispatchHeaders(Header[] headerArr);
}
