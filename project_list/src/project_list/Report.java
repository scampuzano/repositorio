package project_list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Report implements Serializable {
    private static final long serialVersionUID = 4070507633958923271L;
    protected LstPensionados lstPensionados;
    protected String name;
    protected String textbox9;

    public static class LstPensionados implements Serializable {
        private static final long serialVersionUID = -1582470614297251867L;
        protected List<LstPensionadosDetailsGroup> lstPensionadosDetailsGroup;

        public static class LstPensionadosDetailsGroup implements Serializable {
            private static final long serialVersionUID = 7974822052385254307L;
            protected TblAsistenciaSocial tblAsistenciaSocial;
            protected TblCesantias tblCesantias;
            protected TblCompensacionFamiliar tblCompensacionFamiliar;
            protected TblPensionados tblPensionados;
            protected TblPensiones tblPensiones;
            protected String textbox102;
            protected String textbox103;
            protected String textbox13;
            protected String textbox15;
            protected String textbox3;
            protected String textbox4;
            protected String textbox86;
            protected String textbox87;

            public static class TblAsistenciaSocial implements Serializable {
                private static final long serialVersionUID = -3895719142094587143L;
                protected TblAsistenciaSocialGrpPersonaCollection tblAsistenciaSocialGrpPersonaCollection;

                public static class TblAsistenciaSocialGrpPersonaCollection implements Serializable {
                    private static final long serialVersionUID = -7032756934100395719L;
                    protected List<TblAsistenciaSocialGrpPersona> tblAsistenciaSocialGrpPersona;

                    public static class TblAsistenciaSocialGrpPersona implements Serializable {
                        private static final long serialVersionUID = 9045442831035584629L;
                        protected TblAsistenciaSocialGrpAsistenciaSocialCollection tblAsistenciaSocialGrpAsistenciaSocialCollection;

                        public static class TblAsistenciaSocialGrpAsistenciaSocialCollection implements Serializable {
                            private static final long serialVersionUID = -9060387259728425638L;
                            protected List<TblAsistenciaSocialGrpAsistenciaSocial> tblAsistenciaSocialGrpAsistenciaSocial;

                            public static class TblAsistenciaSocialGrpAsistenciaSocial implements Serializable {
                                private static final long serialVersionUID = 7420291895817750707L;
                                protected String estadoVinculacionAS;
                                protected String fechaEntregaUltimoBeneficioAS;
                                protected String textbox101;
                                protected String textbox21;
                                protected String textbox22;
                                protected String textbox23;
                                protected String textbox24;
                                protected String textbox25;
                                protected String textbox85;
                                protected String valorBeneficioAS;

                                public String getFechaEntregaUltimoBeneficioAS() {
                                    return this.fechaEntregaUltimoBeneficioAS;
                                }

                                public void setFechaEntregaUltimoBeneficioAS(String value) {
                                    this.fechaEntregaUltimoBeneficioAS = value;
                                }

                                public String getTextbox85() {
                                    return this.textbox85;
                                }

                                public void setTextbox85(String value) {
                                    this.textbox85 = value;
                                }

                                public String getValorBeneficioAS() {
                                    return this.valorBeneficioAS;
                                }

                                public void setValorBeneficioAS(String value) {
                                    this.valorBeneficioAS = value;
                                }

                                public String getTextbox101() {
                                    return this.textbox101;
                                }

                                public void setTextbox101(String value) {
                                    this.textbox101 = value;
                                }

                                public String getTextbox24() {
                                    return this.textbox24;
                                }

                                public void setTextbox24(String value) {
                                    this.textbox24 = value;
                                }

                                public String getTextbox25() {
                                    return this.textbox25;
                                }

                                public void setTextbox25(String value) {
                                    this.textbox25 = value;
                                }

                                public String getTextbox21() {
                                    return this.textbox21;
                                }

                                public void setTextbox21(String value) {
                                    this.textbox21 = value;
                                }

                                public String getTextbox22() {
                                    return this.textbox22;
                                }

                                public void setTextbox22(String value) {
                                    this.textbox22 = value;
                                }

                                public String getTextbox23() {
                                    return this.textbox23;
                                }

                                public void setTextbox23(String value) {
                                    this.textbox23 = value;
                                }

                                public String getEstadoVinculacionAS() {
                                    return this.estadoVinculacionAS;
                                }

                                public void setEstadoVinculacionAS(String value) {
                                    this.estadoVinculacionAS = value;
                                }
                            }

                            public List<TblAsistenciaSocialGrpAsistenciaSocial> getTblAsistenciaSocialGrpAsistenciaSocial() {
                                if (this.tblAsistenciaSocialGrpAsistenciaSocial == null) {
                                    this.tblAsistenciaSocialGrpAsistenciaSocial = new ArrayList();
                                }
                                return this.tblAsistenciaSocialGrpAsistenciaSocial;
                            }
                        }

                        public TblAsistenciaSocialGrpAsistenciaSocialCollection getTblAsistenciaSocialGrpAsistenciaSocialCollection() {
                            return this.tblAsistenciaSocialGrpAsistenciaSocialCollection;
                        }

                        public void setTblAsistenciaSocialGrpAsistenciaSocialCollection(TblAsistenciaSocialGrpAsistenciaSocialCollection value) {
                            this.tblAsistenciaSocialGrpAsistenciaSocialCollection = value;
                        }
                    }

                    public List<TblAsistenciaSocialGrpPersona> getTblAsistenciaSocialGrpPersona() {
                        if (this.tblAsistenciaSocialGrpPersona == null) {
                            this.tblAsistenciaSocialGrpPersona = new ArrayList();
                        }
                        return this.tblAsistenciaSocialGrpPersona;
                    }
                }

                public TblAsistenciaSocialGrpPersonaCollection getTblAsistenciaSocialGrpPersonaCollection() {
                    return this.tblAsistenciaSocialGrpPersonaCollection;
                }

                public void setTblAsistenciaSocialGrpPersonaCollection(TblAsistenciaSocialGrpPersonaCollection value) {
                    this.tblAsistenciaSocialGrpPersonaCollection = value;
                }
            }

            public static class TblCesantias implements Serializable {
                private static final long serialVersionUID = 7927321387644682490L;
                protected TblCesantiasGrpCesantiasCollection tblCesantiasGrpCesantiasCollection;

                public static class TblCesantiasGrpCesantiasCollection implements Serializable {
                    private static final long serialVersionUID = -7253973171983319071L;
                    protected List<TblCesantiasGrpCesantias> tblCesantiasGrpCesantias;

                    public static class TblCesantiasGrpCesantias implements Serializable {
                        private static final long serialVersionUID = -1348086424624901880L;
                        protected String textbox90;
                        protected String textbox91;
                        protected String textbox92;
                        protected String textbox93;
                        protected String textbox94;

                        public String getTextbox90() {
                            return this.textbox90;
                        }

                        public void setTextbox90(String value) {
                            this.textbox90 = value;
                        }

                        public String getTextbox91() {
                            return this.textbox91;
                        }

                        public void setTextbox91(String value) {
                            this.textbox91 = value;
                        }

                        public String getTextbox92() {
                            return this.textbox92;
                        }

                        public void setTextbox92(String value) {
                            this.textbox92 = value;
                        }

                        public String getTextbox93() {
                            return this.textbox93;
                        }

                        public void setTextbox93(String value) {
                            this.textbox93 = value;
                        }

                        public String getTextbox94() {
                            return this.textbox94;
                        }

                        public void setTextbox94(String value) {
                            this.textbox94 = value;
                        }
                    }

                    public List<TblCesantiasGrpCesantias> getTblCesantiasGrpCesantias() {
                        if (this.tblCesantiasGrpCesantias == null) {
                            this.tblCesantiasGrpCesantias = new ArrayList();
                        }
                        return this.tblCesantiasGrpCesantias;
                    }
                }

                public TblCesantiasGrpCesantiasCollection getTblCesantiasGrpCesantiasCollection() {
                    return this.tblCesantiasGrpCesantiasCollection;
                }

                public void setTblCesantiasGrpCesantiasCollection(TblCesantiasGrpCesantiasCollection value) {
                    this.tblCesantiasGrpCesantiasCollection = value;
                }
            }

            public static class TblCompensacionFamiliar implements Serializable {
                private static final long serialVersionUID = -1089278368841681828L;
                protected TblCompensacionFamiliarGrpCompensacionFamiliarCollection tblCompensacionFamiliarGrpCompensacionFamiliarCollection;

                public static class TblCompensacionFamiliarGrpCompensacionFamiliarCollection implements Serializable {
                    private static final long serialVersionUID = 1212755844621162366L;
                    protected List<TblCompensacionFamiliarGrpCompensacionFamiliar> tblCompensacionFamiliarGrpCompensacionFamiliar;

                    public static class TblCompensacionFamiliarGrpCompensacionFamiliar implements Serializable {
                        private static final long serialVersionUID = 3908093886102990167L;
                        protected String departamentoLaboralCF;
                        protected String textbox59;
                        protected String textbox60;
                        protected String textbox61;
                        protected String tipoAfiliadoCF;
                        protected String tipoMPCCF;

                        public String getTipoAfiliadoCF() {
                            return this.tipoAfiliadoCF;
                        }

                        public void setTipoAfiliadoCF(String value) {
                            this.tipoAfiliadoCF = value;
                        }

                        public String getDepartamentoLaboralCF() {
                            return this.departamentoLaboralCF;
                        }

                        public void setDepartamentoLaboralCF(String value) {
                            this.departamentoLaboralCF = value;
                        }

                        public String getTextbox60() {
                            return this.textbox60;
                        }

                        public void setTextbox60(String value) {
                            this.textbox60 = value;
                        }

                        public String getTextbox61() {
                            return this.textbox61;
                        }

                        public void setTextbox61(String value) {
                            this.textbox61 = value;
                        }

                        public String getTextbox59() {
                            return this.textbox59;
                        }

                        public void setTextbox59(String value) {
                            this.textbox59 = value;
                        }

                        public String getTipoMPCCF() {
                            return this.tipoMPCCF;
                        }

                        public void setTipoMPCCF(String value) {
                            this.tipoMPCCF = value;
                        }
                    }

                    public List<TblCompensacionFamiliarGrpCompensacionFamiliar> getTblCompensacionFamiliarGrpCompensacionFamiliar() {
                        if (this.tblCompensacionFamiliarGrpCompensacionFamiliar == null) {
                            this.tblCompensacionFamiliarGrpCompensacionFamiliar = new ArrayList();
                        }
                        return this.tblCompensacionFamiliarGrpCompensacionFamiliar;
                    }
                }

                public TblCompensacionFamiliarGrpCompensacionFamiliarCollection getTblCompensacionFamiliarGrpCompensacionFamiliarCollection() {
                    return this.tblCompensacionFamiliarGrpCompensacionFamiliarCollection;
                }

                public void setTblCompensacionFamiliarGrpCompensacionFamiliarCollection(TblCompensacionFamiliarGrpCompensacionFamiliarCollection value) {
                    this.tblCompensacionFamiliarGrpCompensacionFamiliarCollection = value;
                }
            }

            public static class TblPensionados implements Serializable {
                private static final long serialVersionUID = -7262577691245991395L;
                protected TblPensionadosGroup1Collection tblPensionadosGroup1Collection;

                public static class TblPensionadosGroup1Collection implements Serializable {
                    private static final long serialVersionUID = 2632733512845686918L;
                    protected List<TblPensionadosGroup1> tblPensionadosGroup1;

                    public static class TblPensionadosGroup1 implements Serializable {
                        private static final long serialVersionUID = -4761513678125181555L;
                        protected String modalidadPensionPG;
                        protected String numeroResolucionPensionPG;
                        protected String pensionCompartidaPG;
                        protected String textbox62;
                        protected String textbox63;
                        protected String textbox69;
                        protected String textbox70;
                        protected String textbox71;

                        public java.lang.String getNumeroResolucionPensionPG() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.numeroResolucionPensionPG;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getNumeroResolucionPensionPG():java.lang.String");
                        }

                        public void setNumeroResolucionPensionPG(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.numeroResolucionPensionPG = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setNumeroResolucionPensionPG(java.lang.String):void");
                        }

                        public java.lang.String getPensionCompartidaPG() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.pensionCompartidaPG;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getPensionCompartidaPG():java.lang.String");
                        }

                        public void setPensionCompartidaPG(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.pensionCompartidaPG = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setPensionCompartidaPG(java.lang.String):void");
                        }

                        public java.lang.String getTextbox69() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox69;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getTextbox69():java.lang.String");
                        }

                        public void setTextbox69(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox69 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setTextbox69(java.lang.String):void");
                        }

                        public java.lang.String getTextbox70() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox70;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getTextbox70():java.lang.String");
                        }

                        public void setTextbox70(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox70 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setTextbox70(java.lang.String):void");
                        }

                        public java.lang.String getTextbox71() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox71;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getTextbox71():java.lang.String");
                        }

                        public void setTextbox71(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox71 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setTextbox71(java.lang.String):void");
                        }

                        public java.lang.String getTextbox62() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox62;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getTextbox62():java.lang.String");
                        }

                        public void setTextbox62(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox62 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setTextbox62(java.lang.String):void");
                        }

                        public java.lang.String getTextbox63() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox63;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getTextbox63():java.lang.String");
                        }

                        public void setTextbox63(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox63 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setTextbox63(java.lang.String):void");
                        }

                        public java.lang.String getModalidadPensionPG() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.modalidadPensionPG;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.getModalidadPensionPG():java.lang.String");
                        }

                        public void setModalidadPensionPG(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.modalidadPensionPG = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensionados.TblPensionadosGroup1Collection.TblPensionadosGroup1.setModalidadPensionPG(java.lang.String):void");
                        }
                    }


                }




            }

            public static class TblPensiones implements Serializable {
                private static final long serialVersionUID = -6528988615117150446L;
                protected TblPensionesGrpPensionesCollection tblPensionesGrpPensionesCollection;

                public static class TblPensionesGrpPensionesCollection implements Serializable {
                    private static final long serialVersionUID = -6070967121183583715L;
                    protected List<TblPensionesGrpPensiones> tblPensionesGrpPensiones;

                    public static class TblPensionesGrpPensiones implements Serializable {
                        private static final long serialVersionUID = 9143273579774637278L;
                        protected String textbox16;
                        protected String textbox17;
                        protected String textbox18;
                        protected String textbox20;
                        protected String textbox39;

                        public java.lang.String getTextbox16() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox16;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.getTextbox16():java.lang.String");
                        }

                        public void setTextbox16(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox16 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.setTextbox16(java.lang.String):void");
                        }

                        public java.lang.String getTextbox17() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox17;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.getTextbox17():java.lang.String");
                        }

                        public void setTextbox17(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox17 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.setTextbox17(java.lang.String):void");
                        }

                        public java.lang.String getTextbox20() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox20;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.getTextbox20():java.lang.String");
                        }

                        public void setTextbox20(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox20 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.setTextbox20(java.lang.String):void");
                        }

                        public java.lang.String getTextbox18() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox18;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.getTextbox18():java.lang.String");
                        }

                        public void setTextbox18(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox18 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.setTextbox18(java.lang.String):void");
                        }

                        public java.lang.String getTextbox39() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox39;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.getTextbox39():java.lang.String");
                        }

                        public void setTextbox39(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox39 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblPensiones.TblPensionesGrpPensionesCollection.TblPensionesGrpPensiones.setTextbox39(java.lang.String):void");
                        }
                    }






            public static class TblPersona implements Serializable {
                private static final long serialVersionUID = -3772994808682478759L;
                protected TblPersonaGrpPersonaCollection tblPersonaGrpPersonaCollection;

                public static class TblPersonaGrpPersonaCollection implements Serializable {
                    private static final long serialVersionUID = 3227018321170956307L;
                    protected List<TblPersonaGrpPersona> tblPersonaGrpPersona;

                    public static class TblPersonaGrpPersona implements Serializable {
                        private static final long serialVersionUID = 6198722974837556807L;
                        protected String primerNombre;
                        protected String sexo;
                        protected String tipoIdentificacion;

                        public String getTipoIdentificacion() {
                            return this.tipoIdentificacion;
                        }

                        public void setTipoIdentificacion(String value) {
                            this.tipoIdentificacion = value;
                        }

                        public String getPrimerNombre() {
                            return this.primerNombre;
                        }

                        public void setPrimerNombre(String value) {
                            this.primerNombre = value;
                        }

                        public String getSexo() {
                            return this.sexo;
                        }

                        public void setSexo(String value) {
                            this.sexo = value;
                        }
                    }

                    public List<TblPersonaGrpPersona> getTblPersonaGrpPersona() {
                        if (this.tblPersonaGrpPersona == null) {
                            this.tblPersonaGrpPersona = new ArrayList();
                        }
                        return this.tblPersonaGrpPersona;
                    }
                }

                public TblPersonaGrpPersonaCollection getTblPersonaGrpPersonaCollection() {
                    return this.tblPersonaGrpPersonaCollection;
                }

                public void setTblPersonaGrpPersonaCollection(TblPersonaGrpPersonaCollection value) {
                    this.tblPersonaGrpPersonaCollection = value;
                }
            }

            public static class TblRiesgosProfesionales implements Serializable {
                private static final long serialVersionUID = 4527635039223057809L;
                protected TblRiesgosProfesionalesGrpRiegosProfesionalesCollection tblRiesgosProfesionalesGrpRiegosProfesionalesCollection;

                public static class TblRiesgosProfesionalesGrpRiegosProfesionalesCollection implements Serializable {
                    private static final long serialVersionUID = 6056823275782794637L;
                    protected List<TblRiesgosProfesionalesGrpRiegosProfesionales> tblRiesgosProfesionalesGrpRiegosProfesionales;

                    public static class TblRiesgosProfesionalesGrpRiegosProfesionales implements Serializable {
                        private static final long serialVersionUID = 2712976572059015026L;
                        protected String departamentoLaboralRP;
                        protected String textbox47;
                        protected String textbox48;
                        protected String textbox49;
                        protected String textbox50;
                        protected String textbox51;

                        public java.lang.String getTextbox48() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox48;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.getTextbox48():java.lang.String");
                        }

                        public void setTextbox48(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox48 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.setTextbox48(java.lang.String):void");
                        }

                        public java.lang.String getTextbox49() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox49;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.getTextbox49():java.lang.String");
                        }

                        public void setTextbox49(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox49 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.setTextbox49(java.lang.String):void");
                        }

                        public java.lang.String getTextbox51() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox51;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.getTextbox51():java.lang.String");
                        }

                        public void setTextbox51(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox51 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.setTextbox51(java.lang.String):void");
                        }

                        public java.lang.String getDepartamentoLaboralRP() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.departamentoLaboralRP;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.getDepartamentoLaboralRP():java.lang.String");
                        }

                        public void setDepartamentoLaboralRP(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.departamentoLaboralRP = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.setDepartamentoLaboralRP(java.lang.String):void");
                        }

                        public java.lang.String getTextbox50() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox50;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.getTextbox50():java.lang.String");
                        }

                        public void setTextbox50(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox50 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.setTextbox50(java.lang.String):void");
                        }

                        public java.lang.String getTextbox47() {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r1 = this;
                            r0 = r1.textbox47;
                            return r0;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.getTextbox47():java.lang.String");
                        }

                        public void setTextbox47(java.lang.String r1) {
                            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: SSA rename variables already executed
	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:120)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:52)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                            /*
                            r0 = this;
                            r0.textbox47 = r1;
                            return;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.datos.Report.LstPensionados.LstPensionadosDetailsGroup.TblRiesgosProfesionales.TblRiesgosProfesionalesGrpRiegosProfesionalesCollection.TblRiesgosProfesionalesGrpRiegosProfesionales.setTextbox47(java.lang.String):void");
                        }
                    

                    



            public static class TblSL implements Serializable {
                private static final long serialVersionUID = -1630395470030052645L;
                protected TblSLGrpSLCollection tblSLGrpSLCollection;

                public static class TblSLGrpSLCollection implements Serializable {
                    private static final long serialVersionUID = 9178131852074692471L;
                    protected List<TblSLGrpSL> tblSLGrpSL;

                    public static class TblSLGrpSL implements Serializable {
                        private static final long serialVersionUID = -4765093275356765197L;
                        protected String administradoraSL;
                        protected String estadoAfiliadoSL;
                        protected String fechaAfiliacionSL;
                        protected String regimenSL;
                        protected String textbox81;
                        protected String tipoAfiliadoSL;

                        public String getRegimenSL() {
                            return this.regimenSL;
                        }

               

                        public String getAdministradoraSL() {
                            return this.administradoraSL;
                        }


                        public String getFechaAfiliacionSL() {
                            return this.fechaAfiliacionSL;
                        }

                        public String getEstadoAfiliadoSL() {
                            return this.estadoAfiliadoSL;
                        }

     

                        public String getTipoAfiliadoSL() {
                            return this.tipoAfiliadoSL;
                        }

           

                        public String getTextbox81() {
                            return this.textbox81;
                        }

         
                    }

                    public List<TblSLGrpSL> getTblSLGrpSL() {
                        if (this.tblSLGrpSL == null) {
                            this.tblSLGrpSL = new ArrayList();
                        }
                        return this.tblSLGrpSL;
                    }
                }

                public TblSLGrpSLCollection getTblSLGrpSLCollection() {
                    return this.tblSLGrpSLCollection;
                }


        }
                
           
         

    



