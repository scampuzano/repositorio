package se.verifique.app.android.datos;

import com.itextpdf.text.pdf.ByteBuffer;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;




public class CedulaColombiana implements Serializable {
    private static final int CEDULA_TIPO_02 = 2;
    private static final int CEDULA_TIPO_03 = 3;
    private static final int TARJETA_IDENTIDAD_TIPO_I3 = 4;
    private static final long serialVersionUID = 6355349373843186253L;
    private String datos;
    private String fechaNacimiento;
    private String fechaNacimientoFormato2;
    private int numero;
    private transient Parametros parametros;
    private String primerApellido;
    private String primerNombre;
    private byte[] rawData;
    private String segundoApellido;
    private String segundoNombre;
    private int tipo;
    private String tipoSangre;

    public String getNumero() {
        return "" + this.numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    private String decodeUTF8(byte[] bytes) {
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public CedulaColombiana(byte[] rawData, Parametros parametros) {
        this.rawData = rawData;
        this.datos = decodeUTF8(rawData);
        this.parametros = parametros;
    }

    private int getTipoCedula() {
        if (this.rawData[0] == ByteBuffer.ZERO && this.rawData[1] == (byte) 50) {
            return 2;
        }
        if (this.rawData[0] == ByteBuffer.ZERO && this.rawData[1] == (byte) 51) {
            return 3;
        }
        if (this.rawData[0] == (byte) 73 && this.rawData[1] == (byte) 51) {
            return 4;
        }
        return 0;
    }

    private String getTextoData(int i, int j) {
        String texto = "";
        int posicion = (i + j) - 1;
        while (posicion >= i && this.datos.charAt(posicion) == '\u0000' && this.datos.charAt(posicion) == TokenParser.SP) {
            posicion--;
        }
        int k = i;
        while (k <= posicion) {
            if (!(this.datos.charAt(k) == '\u0000' || this.datos.charAt(k) == TokenParser.SP)) {
                texto = texto + this.datos.charAt(k);
            }
            k++;
        }
        return texto;
    }

    public void procesarCedula() throws VerifiqueseException {
        if (this.rawData.length < 531) {
            throw new VerifiqueseException(this.parametros.getString(C1167R.string.mensaje_error_cedula_1));
        }
        try {
            int delta;
            this.numero = Integer.parseInt(getTextoData(48, 10));
            this.tipo = getTipoCedula();
            switch (this.tipo) {
                case 2:
                    try {
                        Long.parseLong(getTextoData(22, 12));
                        delta = 0;
                        break;
                    } catch (NumberFormatException e) {
                        throw new VerifiqueseException(this.parametros.getString(C1167R.string.mensaje_error_cedula_2));
                    }
                case 3:
                    if ("PubDSK_1".equals(getTextoData(24, 8))) {
                        delta = 0;
                        break;
                    }
                    throw new VerifiqueseException(this.parametros.getString(C1167R.string.mensaje_error_cedula_2));
                case 4:
                    if ("PubDSK_1".equals(getTextoData(24, 8))) {
                        delta = 1;
                        break;
                    }
                    throw new VerifiqueseException(this.parametros.getString(C1167R.string.mensaje_error_cedula_4));
                default:
                    throw new VerifiqueseException(this.parametros.getString(C1167R.string.mensaje_error_cedula_3));
            }
            this.primerApellido = getTextoData(delta + 58, 23);
            this.segundoApellido = getTextoData(delta + 81, 23);
            this.primerNombre = getTextoData(delta + 104, 23);
            this.segundoNombre = getTextoData(delta + 127, 23);
            setFechaNacimiento(getTextoData(delta + 152, 8));
            setFechaNacimientoFormato2(this.fechaNacimiento);
            this.fechaNacimiento = this.fechaNacimiento.substring(6, 8) + "-" + getMesTexto(this.fechaNacimiento.substring(4, 6)) + "-" + this.fechaNacimiento.substring(0, 4);
            this.tipoSangre = getTextoData(delta + 166, 2);
        } catch (NumberFormatException e2) {
            this.numero = 0;
            throw new VerifiqueseException(this.parametros.getString(C1167R.string.mensaje_error_cedula_2));
        }
    }

    private String getMesTexto(String mes) {
        if ("01".equals(mes)) {
            return "ENE";
        }
        if ("02".equals(mes)) {
            return "FEB";
        }
        if ("03".equals(mes)) {
            return "MAR";
        }
        if ("04".equals(mes)) {
            return "ABR";
        }
        if ("05".equals(mes)) {
            return "MAY";
        }
        if ("06".equals(mes)) {
            return "JUN";
        }
        if ("07".equals(mes)) {
            return "JUL";
        }
        if ("08".equals(mes)) {
            return "AGO";
        }
        if ("09".equals(mes)) {
            return "SEP";
        }
        if ("10".equals(mes)) {
            return "OCT";
        }
        if ("11".equals(mes)) {
            return "NOV";
        }
        if ("12".equals(mes)) {
            return "DIC";
        }
        return "";
    }

    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTipoSangre() {
        return this.tipoSangre;
    }

    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    public String getFechaNacimientoFormato2() {
        return this.fechaNacimientoFormato2;
    }

    public void setFechaNacimientoFormato2(String fechaNacimientoFormato2) {
        this.fechaNacimientoFormato2 = fechaNacimientoFormato2;
    }
}
