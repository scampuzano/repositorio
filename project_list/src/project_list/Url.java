package project_list;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;




public class Url {

	private URL url;
	private String data;
	private int codigoOC = 0;
	private String poID = null;
	private String status = null;
	
	


	public Url(String url) throws MalformedURLException {
		this.url = new URL(url);
		data = "";
	}

	public void add(String propiedad, String valor) throws UnsupportedEncodingException {
		// codificamos cada uno de los valores
		if (data.length() > 0)
			data += "&" + URLEncoder.encode(propiedad, "UTF-8") + "=" + URLEncoder.encode(valor, "UTF-8");
		else
			data += URLEncoder.encode(propiedad, "UTF-8") + "=" + URLEncoder.encode(valor, "UTF-8");
		try {
			if (propiedad.equals("pv_po_id")) {
				codigoOC = Integer.parseInt(valor);
			}
			if (propiedad.equals("po_id")) {
				poID = valor;
			}
			if (propiedad.equals("status")) {
				status = valor;
			}
		} catch (Exception e) {}
	}
	
	public String getResponseGET() throws IOException {
        HttpsURLConnection conn = null;

        conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

    	
        if (data.length() != 0) {
            try (OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream())) {
                out.write(data);
                out.flush();
                out.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        String resultado = "";
        try {
        	BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String currentLine;
            while ((currentLine = in.readLine()) != null) {
                resultado += currentLine;
            }
            return resultado;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
	}

	
	public String getResponse() throws IOException {
        String resultado = "";
        try {
	        HttpsURLConnection conn = null;
	
	        conn = (HttpsURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setInstanceFollowRedirects(true);
	        conn.setRequestMethod("POST");
	
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("charset", "UTF-8");
	        conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
	
	        conn.setUseCaches(false);
        	OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(data);
            out.flush();
            out.close();
            
        	BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String currentLine;
            while ((currentLine = in.readLine()) != null) {
                resultado += currentLine;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            resultado = ex.toString();
        }
        return resultado;
	}

	public String getRespuesta() throws IOException {
		String respuesta = "";
		try {
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setConnectTimeout(10000);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			
			wr.writeBytes(data);
			wr.flush();
			wr.close();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String linea = rd.readLine();
			while (linea != null) {
				respuesta += linea;
				linea = rd.readLine();
			}
		} catch (Exception e) {
			respuesta = e.toString();
		}
		return respuesta;
	}
	


	public URL getUrl() {
		return url;
	}

	public String getData() {
		return data;
	}

	public int getCodigoOC() {
		return codigoOC;
	}

	public void setCodigoOC(int codigoOC) {
		this.codigoOC = codigoOC;
	}

	public String getPoID() {
		return poID;
	}

	public void setPoID(String poID) {
		this.poID = poID;
	}

	public String getStatus() {
		return status;
	}
}
