package project_list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.apache.http.message.BasicNameValuePair;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public class Metodos {
	
    public class HeadersListenerImpl implements HeadersListener {
        public Header[] headers;

        public void dispatchHeaders(Header[] headers) {
            this.headers = headers;
        }
    }
	
	
	   public Persona getRUAF(Persona persona) throws VerifiqueseException {
	        String html = ServicioHTTP.getHTML("http://ruafsvr2.sispro.gov.co/RUAF/Cliente/WebPublico/Consultas/D04AfiliacionesPersonaRUAF.aspx", 1, null, null, null, null, null, false, null, false);
	            if ("VFQ-TO".equals(html) || "ERROR".equals(html)) {
	            throw new VerifiqueseException(html);
	        }
	        String VIEWSTATEGENERATOR = ServicioHTTP.getRegex(html, "<input type=\"hidden\" name=\"__VIEWSTATEGENERATOR\" id=\"__VIEWSTATEGENERATOR\" value=\"(.*?)\" />");
	        String VIEWSTATE = ServicioHTTP.getRegex(html, "<input type=\"hidden\" name=\"__VIEWSTATE\" id=\"__VIEWSTATE\" value=\"(.*?)\" />");
	        List<NameValuePair> parametros = new ArrayList();
	        parametros.add(new BasicNameValuePair("ctl00$tlkScriptManager", "ctl00$cntContenido$UpdatePanel1|ctl00$cntContenido$btnConsultar"));
	        parametros.add(new BasicNameValuePair("ctl00_tlkScriptManager_HiddenField", ""));
	        parametros.add(new BasicNameValuePair("__EVENTTARGET", ""));
	        parametros.add(new BasicNameValuePair("__EVENTARGUMENT", ""));
	        parametros.add(new BasicNameValuePair("__VIEWSTATE", VIEWSTATE));
	        parametros.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", VIEWSTATEGENERATOR));
	        parametros.add(new BasicNameValuePair("ctl00$cntContenido$ddlTipoIdentificacion", "5"));
	        parametros.add(new BasicNameValuePair("ctl00$cntContenido$txtNumeroIdentificacion", persona.getNumeroCedula()));
	        parametros.add(new BasicNameValuePair("__ASYNCPOST", PdfBoolean.TRUE));
	        parametros.add(new BasicNameValuePair("ctl00$cntContenido$btnConsultar", "Consultar"));
	        HeadersListenerImpl hli = new HeadersListenerImpl();
	        Map<String, String> parametrosHeaders = new HashMap();
	        parametrosHeaders.put("HANDLE_REDIRECTS_VERIFIQUESE", "NO");
	        parametrosHeaders.put("X-MicrosoftAjax", "Delta=true");
	        parametrosHeaders.put("Accept-Language", "es-''ar,es;q=0.8,en-us;q=0.5,en;q=0.3");
	        parametrosHeaders.put(HttpHeaders.X_REQUESTED_WITH, "XMLHttpRequest");
	        parametrosHeaders.put("Referer", "http://ruafsvr2.sispro.gov.co/RUAF/Cliente/WebPublico/Consultas/D04AfiliacionesPersonaRUAF.aspx");
	        parametrosHeaders.put("Accept-Encoding", "gzip, deflate");
	        parametrosHeaders.put("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
	        parametrosHeaders.put("Connection", "keep-alive");
	        parametrosHeaders.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
	        parametrosHeaders.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	        parametrosHeaders.put("Cache-Control", "no-cache");
	        parametrosHeaders.put("Pragma", "no-cache");
	        html = ServicioHTTP.getHTML("http://ruafsvr2.sispro.gov.co/RUAF/Cliente/WebPublico/Consultas/D04AfiliacionesPersonaRUAF.aspx", 2, parametros, null, hli, null, parametrosHeaders, false, null, false);
	        if ("VFQ-TO".equals(html) || "ERROR".equals(html)) {
	            throw new VerifiqueseException(html);
	        }
	        int i;
	        Map<String, String> cookies = new HashMap();
	        cookies.put("VerifiqueseDominio", "ruafsvr2.sispro.gov.co");
	        cookies.put("VerifiquesePath", "/");
	        for (i = 0; i < hli.headers.length; i++) {
	            String texto;
	            if (HttpHeaders.SET_COOKIE.equals(hli.headers[i].getName())) {
	                texto = "";
	                if (hli.headers[i].getValue().indexOf(";") > 0) {
	                    texto = hli.headers[i].getValue().substring(0, hli.headers[i].getValue().indexOf(";"));
	                } else {
	                    texto = hli.headers[i].getValue();
	                }
	                String str = texto;
	                cookies.put(texto.substring(0, texto.indexOf(Operation.EQUALS)), str.substring(texto.indexOf(Operation.EQUALS) + 1, texto.length()));
	            }
	        }
	        String GUID = ServicioHTTP.getRegex(html, "GUID=(.*?)\"");
	        new ArrayList().add(new BasicNameValuePair("GUID", GUID));
	        parametrosHeaders = new HashMap();
	        parametrosHeaders.put("HANDLE_REDIRECTS_VERIFIQUESE", "NO");
	        parametrosHeaders.put("Accept-Language", "es-ar,es;q=0.8,en-us;q=0.5,en;q=0.3");
	        parametrosHeaders.put("Referer", "http://ruafsvr2.sispro.gov.co/RUAF/Cliente/WebPublico/Consultas/D04AfiliacionesPersonaRUAF.aspx");
	        parametrosHeaders.put("Accept-Encoding", "gzip, deflate");
	        parametrosHeaders.put("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
	        parametrosHeaders.put("Connection", "close");
	        parametrosHeaders.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	        html = ServicioHTTP.getHTML("http://ruafsvr2.sispro.gov.co/RUAF/Cliente/WebPublico/Visor/frmVisorReporte.aspx?GUID=" + GUID, 1, null, cookies, null, null, parametrosHeaders, false, null, false);
	        if ("VFQ-TO".equals(html) || "ERROR".equals(html)) {
	            throw new VerifiqueseException(html);
	        }
	        String reportSession = ServicioHTTP.getRegex(html, "ReportSession=(.*?)&");
	        String controlID = ServicioHTTP.getRegex(html, "ControlID=(.*?)&");
	        parametrosHeaders = new HashMap();
	        parametrosHeaders.put("HANDLE_REDIRECTS_VERIFIQUESE", "NO");
	        parametrosHeaders.put("Accept-Language", "es-ar,es;q=0.8,en-us;q=0.5,en;q=0.3");
	        parametrosHeaders.put("Referer", "http://ruafsvr2.sispro.gov.co/RUAF/Cliente/WebPublico/Visor/frmVisorReporte.aspx?GUID=" + GUID);
	        parametrosHeaders.put("Accept-Encoding", "gzip, deflate");
	        parametrosHeaders.put("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
	        parametrosHeaders.put("Connection", "close");
	        parametrosHeaders.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	        cookies = new HashMap();
	        cookies.put("VerifiqueseDominio", "ruafsvr2.sispro.gov.co");
	        cookies.put("VerifiquesePath", "/");
	        for (i = 0; i < hli.headers.length; i++) {
	            if (HttpHeaders.SET_COOKIE.equals(hli.headers[i].getName())) {
	                texto = "";
	                if (hli.headers[i].getValue().indexOf(";") > 0) {
	                    texto = hli.headers[i].getValue().substring(0, hli.headers[i].getValue().indexOf(";"));
	                } else {
	                    texto = hli.headers[i].getValue();
	                }
	                str = texto;
	                cookies.put(texto.substring(0, texto.indexOf(Operation.EQUALS)), str.substring(texto.indexOf(Operation.EQUALS) + 1, texto.length()));
	            }
	        }
	        html = ServicioHTTP.getHTML("http://ruafsvr2.sispro.gov.co/RUAF/Cliente/WebPublico/Reserved.ReportViewerWebControl.axd?ReportSession=" + reportSession + "&ControlID=" + controlID + "&Culture=9226&UICulture=9226&ReportStack=1&OpType=Export&FileName=D04AfiliacionesPersonaRUAF&ContentDisposition=OnlyHtmlInline&Format=XML", 1, null, cookies, null, null, parametrosHeaders, false, null, false);
	        if ("VFQ-TO".equals(html) || "ERROR".equals(html)) {
	            throw new VerifiqueseException(html);
	        }
	        persona.setRUAF(new RUAFBuilder(html).getReport());
	        return persona;
	    }
	

}
