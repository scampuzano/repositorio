package project_list;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;

public class Hilo implements Runnable {
    private static String TAG = "ViewWebActivity";
    public boolean error = false;
    public Header[] headers;
    private ParametrosRequestHTTP parametrosRequestHTTP = new ParametrosRequestHTTP();
    public String resultado = "";
    public boolean terminado = false;

    public Hilo(ParametrosRequestHTTP parametrosRequestHTTP) {
        this.parametrosRequestHTTP = parametrosRequestHTTP;
    }

    public Hilo(String urlToRead, int tipoSolicitud, List<NameValuePair> parametros, Map<String, String> cookies, CredentialsProvider credProvider, Map<String, String> headersParametros, boolean isPostData, String postData, boolean descargar) {
        this.parametrosRequestHTTP.setCookies(cookies);
        this.parametrosRequestHTTP.setCredenciales(credProvider);
        this.parametrosRequestHTTP.setDescargar(descargar);
        this.parametrosRequestHTTP.setHeadersParametros(headersParametros);
        this.parametrosRequestHTTP.setParametros(parametros);
        this.parametrosRequestHTTP.setPostData(isPostData);
        this.parametrosRequestHTTP.setPostData(postData);
        this.parametrosRequestHTTP.setTipoSolicitud(tipoSolicitud);
        this.parametrosRequestHTTP.setUrl(urlToRead);
    }

    public boolean isHttps(String url) {
        try {
            return "https".equals(new URL(url).getProtocol());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public DefaultHttpClient getDefaultHtttpClient(String url) {
        return new DefaultHttpClient();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getHTML(java.lang.String r37) {
        /*
        r36 = this;
        r8 = 0;
        r30 = "";
        r17 = r36.getDefaultHtttpClient(r37);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x00e6;
    L_0x0013:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.isEmpty();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 != 0) goto L_0x00e6;
    L_0x0023:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "VerifiqueseDominio";
        r12 = r33.get(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r12 = (java.lang.String) r12;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "VerifiqueseDominio";
        r33.remove(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "VerifiquesePath";
        r26 = r33.get(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r26 = (java.lang.String) r26;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "VerifiquesePath";
        r33.remove(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r10 = new org.apache.http.impl.client.BasicCookieStore;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r10.<init>();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r5 = java.util.Calendar.getInstance();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = 6;
        r34 = 100;
        r0 = r33;
        r1 = r34;
        r5.add(r0, r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r11 = r5.getTime();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.keySet();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r20 = r33.iterator();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x008f:
        r33 = r20.hasNext();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x00d6;
    L_0x0095:
        r24 = r20.next();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r24 = (java.lang.String) r24;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r9 = new org.apache.http.impl.cookie.BasicClientCookie;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCookies();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r1 = r24;
        r33 = r0.get(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = (java.lang.String) r33;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r24;
        r1 = r33;
        r9.<init>(r0, r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r9.setDomain(r12);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r9.setExpiryDate(r11);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r26;
        r9.setPath(r0);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r10.addCookie(r9);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x008f;
    L_0x00c7:
        r13 = move-exception;
        r13.printStackTrace();
        r33 = 1;
        r0 = r33;
        r1 = r36;
        r1.error = r0;
    L_0x00d3:
        r31 = r30;
    L_0x00d5:
        return r31;
    L_0x00d6:
        r33 = r17.getParams();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "http.protocol.cookie-policy";
        r35 = "netscape";
        r33.setParameter(r34, r35);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r17;
        r0.setCookieStore(r10);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x00e6:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCredenciales();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x0103;
    L_0x00f2:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCredenciales();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r17;
        r1 = r33;
        r0.setCredentialsProvider(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x0103:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getTipoSolicitud();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        switch(r33) {
            case 2: goto L_0x025c;
            default: goto L_0x0110;
        };	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x0110:
        r15 = new org.apache.http.client.methods.HttpGet;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getUrl();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r15.<init>(r0);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r25 = r15.getParams();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x01ec;
    L_0x0131:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.keySet();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r20 = r33.iterator();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x0143:
        r33 = r20.hasNext();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x01ec;
    L_0x0149:
        r7 = r20.next();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r7 = (java.lang.String) r7;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = "HANDLE_REDIRECTS_VERIFIQUESE";
        r0 = r33;
        r33 = r0.equals(r7);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x01d3;
    L_0x0159:
        r33 = "SI";
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = r0;
        r34 = r34.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r34;
        r34 = r0.get(r7);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.equals(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x01c4;
    L_0x0171:
        r33 = "http.protocol.handle-redirects";
        r34 = java.lang.Boolean.TRUE;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r25;
        r1 = r33;
        r2 = r34;
        r0.setParameter(r1, r2);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x0143;
    L_0x017f:
        r13 = move-exception;
        r33 = new java.lang.StringBuilder;
        r33.<init>();
        r34 = "[";
        r34 = r33.append(r34);
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;
        r33 = r0;
        r33 = r33.getTipoSolicitud();
        r35 = 1;
        r0 = r33;
        r1 = r35;
        if (r0 != r1) goto L_0x0435;
    L_0x019d:
        r33 = "GET";
    L_0x019f:
        r0 = r34;
        r1 = r33;
        r33 = r0.append(r1);
        r34 = " REQUEST]";
        r33 = r33.append(r34);
        r33 = r33.toString();
        r34 = "Network exception";
        r0 = r33;
        r1 = r34;
        android.util.Log.i(r0, r1, r13);
        r33 = 1;
        r0 = r33;
        r1 = r36;
        r1.error = r0;
        goto L_0x00d3;
    L_0x01c4:
        r33 = "http.protocol.handle-redirects";
        r34 = java.lang.Boolean.FALSE;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r25;
        r1 = r33;
        r2 = r34;
        r0.setParameter(r1, r2);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x0143;
    L_0x01d3:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r33 = r0.get(r7);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = (java.lang.String) r33;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r15.setHeader(r7, r0);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x0143;
    L_0x01ec:
        r0 = r17;
        r29 = r0.execute(r15);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x01f2:
        r33 = "Hilo::";
        r34 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34.<init>();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r35 = "RESULTADO = ";
        r34 = r34.append(r35);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r35 = r29.getStatusLine();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = r34.append(r35);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = r34.toString();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        android.util.Log.v(r33, r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r29.getAllHeaders();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r1 = r36;
        r1.headers = r0;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.isDescargar();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 != 0) goto L_0x0380;
    L_0x0224:
        r33 = r29.getEntity();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r8 = r33.getContent();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r28 = new java.io.BufferedReader;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = new java.io.InputStreamReader;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r0.<init>(r8);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r28;
        r1 = r33;
        r0.<init>(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x023c:
        r22 = r28.readLine();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r22 == 0) goto L_0x0379;
    L_0x0242:
        r33 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33.<init>();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r1 = r30;
        r33 = r0.append(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r1 = r22;
        r33 = r0.append(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r30 = r33.toString();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x023c;
    L_0x025c:
        r16 = new org.apache.http.client.methods.HttpPost;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getUrl();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r16;
        r1 = r33;
        r0.<init>(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r25 = r16.getParams();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.isPostData();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 != 0) goto L_0x02fb;
    L_0x027f:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x02a1;
    L_0x028b:
        r33 = new org.apache.http.client.entity.UrlEncodedFormEntity;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = r0;
        r34 = r34.getParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33.<init>(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r16;
        r1 = r33;
        r0.setEntity(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x02a1:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x035b;
    L_0x02ad:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.keySet();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r20 = r33.iterator();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x02bf:
        r33 = r20.hasNext();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x035b;
    L_0x02c5:
        r7 = r20.next();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r7 = (java.lang.String) r7;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = "HANDLE_REDIRECTS_VERIFIQUESE";
        r0 = r33;
        r33 = r0.equals(r7);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x0340;
    L_0x02d5:
        r33 = "SI";
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = r0;
        r34 = r34.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r34;
        r34 = r0.get(r7);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.equals(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x0332;
    L_0x02ed:
        r33 = "http.protocol.handle-redirects";
        r34 = java.lang.Boolean.TRUE;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r25;
        r1 = r33;
        r2 = r34;
        r0.setParameter(r1, r2);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x02bf;
    L_0x02fb:
        r32 = new org.apache.http.entity.StringEntity;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getPostData();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "UTF-8";
        r32.<init>(r33, r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getCredenciales();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x0329;
    L_0x0318:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.isPostData();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x0329;
    L_0x0324:
        r33 = "application/json; charset=UTF-8";
        r32.setContentType(r33);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
    L_0x0329:
        r0 = r16;
        r1 = r32;
        r0.setEntity(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x02a1;
    L_0x0332:
        r33 = "http.protocol.handle-redirects";
        r34 = java.lang.Boolean.FALSE;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r25;
        r1 = r33;
        r2 = r34;
        r0.setParameter(r1, r2);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x02bf;
    L_0x0340:
        r0 = r36;
        r0 = r0.parametrosRequestHTTP;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r33 = r33.getHeadersParametros();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r33 = r0.get(r7);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = (java.lang.String) r33;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r16;
        r1 = r33;
        r0.setHeader(r7, r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x02bf;
    L_0x035b:
        r14 = r16.getAllHeaders();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r18 = 0;
    L_0x0361:
        r0 = r14.length;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r0;
        r0 = r18;
        r1 = r33;
        if (r0 >= r1) goto L_0x036f;
    L_0x036a:
        r33 = r14[r18];	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r18 = r18 + 1;
        goto L_0x0361;
    L_0x036f:
        r0 = r17;
        r1 = r16;
        r29 = r0.execute(r1);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        goto L_0x01f2;
    L_0x0379:
        r28.close();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r31 = r30;
        goto L_0x00d5;
    L_0x0380:
        r30 = "ERROR";
        r33 = android.os.Environment.getExternalStorageState();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        if (r33 == 0) goto L_0x00d3;
    L_0x0388:
        r33 = TAG;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "DOWNLOAD PDF:: ";
        android.util.Log.v(r33, r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r27 = new java.util.Random;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r27.<init>();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r6 = new java.io.File;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33.<init>();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.append(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r34 = "/Verifiquese/";
        r33 = r33.append(r34);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r33.toString();	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r0 = r33;
        r6.<init>(r0);	 Catch:{ IOException -> 0x00c7, Exception -> 0x017f }
        r33 = r6.exists();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        if (r33 != 0) goto L_0x03bb;
    L_0x03b8:
        r6.mkdir();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
    L_0x03bb:
        r33 = r6.exists();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        if (r33 == 0) goto L_0x00d3;
    L_0x03c1:
        r33 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33.<init>();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r34 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33 = r33.append(r34);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r34 = "/Verifiquese/Documento_";
        r33 = r33.append(r34);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r34 = r27.nextInt();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33 = r33.append(r34);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r34 = ".pdf";
        r33 = r33.append(r34);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r23 = r33.toString();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r3 = new java.io.BufferedInputStream;	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33 = r29.getEntity();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33 = r33.getContent();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r0 = r33;
        r3.<init>(r0);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r4 = new java.io.BufferedOutputStream;	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33 = new java.io.FileOutputStream;	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r34 = new java.io.File;	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r0 = r34;
        r1 = r23;
        r0.<init>(r1);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33.<init>(r34);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r0 = r33;
        r4.<init>(r0);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r21 = 0;
    L_0x040c:
        r19 = r3.read();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r33 = -1;
        r0 = r19;
        r1 = r33;
        if (r0 == r1) goto L_0x0420;
    L_0x0418:
        r0 = r19;
        r4.write(r0);	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r21 = 1;
        goto L_0x040c;
    L_0x0420:
        r3.close();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        r4.close();	 Catch:{ Exception -> 0x0430, IOException -> 0x00c7 }
        if (r21 == 0) goto L_0x042c;
    L_0x0428:
        r30 = r23;
        goto L_0x00d3;
    L_0x042c:
        r30 = "ERROR";
        goto L_0x00d3;
    L_0x0430:
        r13 = move-exception;
        r30 = "ERROR";
        goto L_0x00d3;
    L_0x0435:
        r33 = "POST";
        goto L_0x019f;
        */
        throw new UnsupportedOperationException("Method not decompiled: se.verifique.app.android.util.Hilo.getHTML(java.lang.String):java.lang.String");
    }

    public void run() {
        this.resultado = getHTML(this.parametrosRequestHTTP.getUrl());
        this.terminado = true;
    }
}
