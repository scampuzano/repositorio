package se.verifique.app.android.datos;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class Persona implements Serializable {
    private static Persona persona = null;
    private static final long serialVersionUID = 1;

    private String apellidos;

    private CedulaColombiana cedulaColombiana;

    private String mensajeCopnia;
    private String nombres;
    private String numeroCedula;

    private Report ruaf;

    public static Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }
        return persona;
    }

    public Report getRUAF() {
        return this.ruaf;
    }

    public void setRUAF(Report ruaf) {
        this.ruaf = ruaf;
    }

    public CedulaColombiana getCedulaColombiana() {
        return this.cedulaColombiana;
    }

    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return this.apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNumeroCedula() {
        return this.numeroCedula;
    }

    public void setNumeroCedula(String numeroCedula) {
        this.numeroCedula = numeroCedula;
    }



    public void setCedulaColombiana(CedulaColombiana cedula) {
        this.cedulaColombiana = cedula;
        setNumeroCedula(this.cedulaColombiana.getNumero());
        setNombres(this.cedulaColombiana.getPrimerNombre() + (this.cedulaColombiana.getSegundoNombre() != null ? MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.cedulaColombiana.getSegundoNombre() : ""));
        setApellidos(this.cedulaColombiana.getPrimerApellido() + (this.cedulaColombiana.getSegundoApellido() != null ? MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.cedulaColombiana.getSegundoApellido() : ""));
    }

    public boolean comparar(Persona personaComparar) {
        return getNombres().equals(personaComparar.getNombres().trim()) && getApellidos().equals(personaComparar.getApellidos().trim());
    }



    public String getMensajeCopnia() {
        return this.mensajeCopnia;
    }

    public void setMensajeCopnia(String mensajeCopnia) {
        this.mensajeCopnia = mensajeCopnia;
    }



    public void limpiarRUAF() {
        this.ruaf = null;
    }

    public String getJSON() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("numero", getNumeroCedula());
            if (getCedulaColombiana() != null) {
                obj.put("primernombre", getCedulaColombiana().getPrimerNombre());
                obj.put("primerapellido", getCedulaColombiana().getPrimerApellido());
                obj.put("segundonombre", getCedulaColombiana().getSegundoNombre());
                obj.put("segundoapellido", getCedulaColombiana().getSegundoApellido());
                obj.put("tiposangre", getCedulaColombiana().getTipoSangre());
            } else {
                obj.put("primernombre", getNombres());
                obj.put("primerapellido", getApellidos());
            }
            return obj.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
