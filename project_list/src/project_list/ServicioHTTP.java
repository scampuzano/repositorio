package project_list;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.NameValuePair;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ksoap2.SoapEnvelope;

public class ServicioHTTP {
    public static Hilo hilo;
    private static boolean parar = false;
    public static Thread thread;

    public static String getHTML(ParametrosRequestHTTP parametrosRequestHTTP) {
        return getHTML(parametrosRequestHTTP.getUrl(), parametrosRequestHTTP.getTipoSolicitud(), parametrosRequestHTTP.getParametros(), parametrosRequestHTTP.getCookies(), parametrosRequestHTTP.getHeadersListener(), parametrosRequestHTTP.getCredenciales(), parametrosRequestHTTP.getHeadersParametros(), parametrosRequestHTTP.isPostData(), parametrosRequestHTTP.getPostData(), parametrosRequestHTTP.isDescargar());
    }

    public static synchronized void stopThread() {
        synchronized (ServicioHTTP.class) {
            parar = true;
        }
    }

    public static String getHTML(String urlToRead, int tipoSolicitud, List<NameValuePair> parametros, Map<String, String> cookies, HeadersListener headersListener, CredentialsProvider credProvider, Map<String, String> headersParametros, boolean isPostData, String postData, boolean descargar) {
        parar = false;
        hilo = new Hilo(urlToRead, tipoSolicitud, parametros, cookies, credProvider, headersParametros, isPostData, postData, descargar);
        Thread t = new Thread(hilo);
        thread = t;
        t.start();
        int contador = 0;
        while (!hilo.terminado && contador < SoapEnvelope.VER12 && !parar) {
            try {
                Thread.sleep(500);
                contador++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (parar) {
            thread = null;
            return "ERROR";
        } else if (!hilo.terminado) {
            thread = null;
            return "VFQ-TO";
        } else if (hilo.error) {
            thread = null;
            return "ERROR";
        } else {
            if (headersListener != null) {
                headersListener.dispatchHeaders(hilo.headers);
            }
            thread = null;
            return hilo.resultado;
        }
    }

    public static String getHTML_OLD2(String urlToRead) {
        String result = "";
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(new DefaultHttpClient().execute(new HttpGet(urlToRead)).getEntity().getContent()));
            while (true) {
                String line = rd.readLine();
                if (line == null) {
                    break;
                }
                result = result + line;
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e2) {
//            Log.i("[GET REQUEST]", "Network exception", e2);
        }
        return result;
    }

    public static String getHTML_OLD(String urlToRead) {
        String result = "";
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(urlToRead).openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while (true) {
                String line = rd.readLine();
                if (line == null) {
                    break;
                }
                result = result + line;
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return result;
    }

    public static String getRegex(String html, String patron) {
        return getRegex(html, patron, 1);
    }

    public static String getRegex(String html, String patron, int posicion) {
        Matcher m = Pattern.compile(patron).matcher(html);
        int i = 0;
        while (m.find()) {
            i++;
            if (i == posicion) {
                return m.group(1);
            }
        }
        return "";
    }
}
